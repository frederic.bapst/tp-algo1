package s10;

// This is a version using explicit Object (as if Java was not offering generic types)
public class ObjQueueChained {
  public ObjQueueChained() {}

  public void enqueue (Object elt) {
    // TODO
  }

  public boolean isEmpty() {
    return false; // TODO
  }

  public Object consult() {
    return null; // TODO
  }

  public int size() {
    return 0; // TODO
  }

  public Object dequeue() {
    return null; // TODO
  }
}
