package s17;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Boggle {
  private final char[][] board;
  private final String[] words;

  public Boggle() {
    this("words.txt", "puz.txt");
  }
  
  public Boggle(String wordsFilename, String boardFilename) {
    System.out.println( "Reading files..." );
    String[] words = new String[0];
    char[][] board = new char[0][0];
    try {
      board = readBoard(boardFilename);
      words = readWords(wordsFilename);
      System.out.println("Done! " + words.length + " words read. Grid " + board.length + " x " + board[0].length);
    } catch(IOException e) {
      System.err.println("problem while reading file..." );
      System.err.println(e);
      e.printStackTrace();
    }
    this.board = board;
    this.words = words;
  }

  public Boggle(String[] words, String[] boardLines) {
    this.words = Arrays.copyOf(words, words.length);
    Arrays.sort(this.words);
    int nRow = boardLines.length;
    int nCol = boardLines[0].length();
    board = new char[nRow][nCol];
    for(int i=0; i<nRow; i++)
      board[i] = boardLines[i].toCharArray();
  }

  public List<String> solvePuzzle( ) {
    List<String> matches = new ArrayList<>();
    int nRows = board.length, nColumns = board[0].length;
    // TODO
    return matches;
  }
  
  /**
   * Performs the binary search for word search
   * using one comparison per level.
   * Assumes a and n are OK.
   * @param a the observedOutput array of strings.
   * @param x the string to search for.
   * @return last position examined;
   *     this position either matches x, or x is
   *     a prefix of the mismatch, or there is no
   *     word for which x is a prefix.
   */
  private static int prefixSearch(String[] a, String x) {
    int low = 0;
    int high = a.length - 1;
    
    while(low < high) {
      int mid = (low + high) / 2;
      if (a[mid].compareTo(x) < 0)
        low = mid + 1;
      else
        high = mid;
    }
    return low;
  }

  private String readNonEmptyLine(BufferedReader br) throws IOException {
    String line = br.readLine();
    while(line!=null && line.length()==0)
      line = br.readLine();
    return line;
  }
  
  /**
   * Routine to read the grid.
   * Checks to ensure that the grid is rectangular.
   */
  private char[][] readBoard(String filename) throws IOException {
    try(FileReader r = new FileReader(filename);
        BufferedReader wordStream = new BufferedReader(r);
        ) {
      List<String> list = new ArrayList<>();
      String line = readNonEmptyLine(wordStream);
      if(line==null) return new char[0][0];
      int lineLen=line.length();
      while(line != null) {
        if(line.length() != lineLen) {
          System.out.println("board not rectangular... skipping");
          break;
        }
        list.add(line);
        line = readNonEmptyLine(wordStream);
      }
      char[][] t = new char[list.size()][lineLen];
      for(int i=0; i<t.length; i++) {
        for(int j=0; j<lineLen; j++) {
          t[i][j] = list.get(i).charAt(j);
        }
      }
      return t;
    }
  }

  /**
   * Routine to read the dictionary and sort it.
   */
  private String[] readWords(String filename) throws IOException {
    try(FileReader r = new FileReader(filename);
        BufferedReader wordStream = new BufferedReader(r);
        ) {
      List<String> list = new ArrayList<>();
      String line = wordStream.readLine();
      while(line != null) {
        list.add(line);
        line = wordStream.readLine();
      }
      Collections.sort(list);
      return list.toArray(new String[0]);
    }
  }
  
  // ------------------------------------------------------------
  // Cheap main
  public static void main(String[] args) {
    Boggle p = new Boggle();
    System.out.println("Solving...");
    List<String> matches = p.solvePuzzle();
    System.out.println("\n*** Total matches: " + matches.size());
    System.out.println(matches);
  }
}
