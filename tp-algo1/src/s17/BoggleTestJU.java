package s17;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BoggleTestJU {
  static final String[] boardLines = {
          "fozepdkdnqlhfejdzksc",
          "cfykdxnlorwvfwavbmyq",
          "clxjrgntqhvuowgrtufh",
          "nbdinvalidateryxxuqp",
          "dmmsksjdooncssvrznss",
          "flsjbahawxsalesvwdbl",
          "sqpkimdjzxdeiwqmwxou",
          "wgukamfjqiwkynwizzty",
          "xxehtyvrtklqsgaduhom",
          "smyszwaywwyvtefozafu"
  };
  static final String[] expectedSolutions =
          "disk,invalid,invalidate,valid,validate,donate,date,dater,terse,sink,scale,bask,wood,sale,less,else"
                  .split(",");

  static final String[] words =
          ("disk,invalid,invalidate,valid,validate,donate,date,dater,terse,sink,scale,bask,wood,sale,less,else"
                  +",0th,AAAS,ANSI,ARPA,ASTM,AT&T,Aarhus,Aaron,Ababa,Abbott,Abel,Abelian,Abelson"
                  + ",zoology,zoom,zounds,zucchini,zygote,termite,bank")
                  .split(",");

  @Test void smallSampleGrid() {
    Boggle p = new Boggle(words, boardLines);
    List<String> matches = p.solvePuzzle();
    Collections.sort(matches);
    Arrays.sort(expectedSolutions);
    List<String> expectedResult = new ArrayList<>();
    for(String s: expectedSolutions)
      expectedResult.add(s);
    assertEquals(expectedResult, matches);

  }
}
