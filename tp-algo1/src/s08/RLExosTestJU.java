package s08;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RLExosTestJU {

  CharRecList listA, listB;

  @BeforeEach void setup() {
    listA = fromString("dbca");
    listB = fromString("tuv");
  }

  static CharRecList fromString(String s) {
    CharRecList r = CharRecList.EMPTY_LIST;
    for(int i=s.length()-1; i>=0; i--)
      r = r.withHead(s.charAt(i));
    return r;
  }

  static String fromRecList(CharRecList a) {
    String s = "";
    while(!a.isEmpty()) {
      s += a.head();
      a = a.tail();
    }
    return s;
  }

  @Test void testAppend() {
    CharRecList r = RLExos.append(listA, 'z');
    assertEquals("dbcaz", fromRecList(r));
  }

  @Test void testConcat() {
    CharRecList r = RLExos.concat(listA, listB);
    assertEquals("dbcatuv", fromRecList(r));
    r = RLExos.concat(listB, listA);
    assertEquals("tuvdbca", fromRecList(r));
    r = RLExos.concat(listB, listB);
    assertEquals("tuvtuv", fromRecList(r));
  }

  @Test void testReplaceEach() {
    CharRecList r = RLExos.replaceEach(listA, 'a', 'b');
    assertEquals("dbcb", fromRecList(r));
    CharRecList a = fromString("1a1a1a1a11a");
    r = RLExos.replaceEach(a, '1', 'b');
    assertEquals("bababababba", fromRecList(r));
  }

  @Test void testConsultAt() {
    String s = "tuv";
    for(int i=0; i < s.length(); i++) {
      char c = RLExos.consultAt(listB, i);
      assertEquals(c, s.charAt(i));
    }
  }

  @Test void testQuicksort() {
    CharRecList r = RLExos.quickSort(listA);
    assertEquals("abcd", fromRecList(r));
    r = RLExos.quickSort(listB);
    assertEquals("tuv", fromRecList(r));
  }
}
