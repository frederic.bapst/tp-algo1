package s05;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static s05.RecursionExos.*;

public class RecursionExosTinyTestJU {
  @Test void testModulo() {
    int max = 15;
    for(int i=1; i<max; i++)
      for(int j=1; j<max; j++)
        assertEquals(i%j, modulo(i, j));
  }

  @Test void testFactorial() {
    assertEquals(120, factorial(5));
    assertEquals(6*7*120, factorial(7));
  }

  @Test void testNbOf1sInBinary() {
    int max = 30_000;
    for(int i=0; i<max; i++) {
      String s = Integer.toBinaryString(i);
      int countOnes = 0;
      for(int j=0; j<s.length(); j++)
        if(s.charAt(j) == '1') countOnes++;
      assertEquals(countOnes, nbOf1sInBinary(i));
    }
    assertEquals(120, factorial(5));
    assertEquals(6*7*120, factorial(7));
  }

  @Test void testFibonacci() {
    int[] fib = {0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144};
    for(int i=0; i<fib.length; i++)
      assertEquals(fib[i], fibonacci(i));
  }

  @Test void testSquare() {
    int max = 775;
    for(int i=1; i<max; i++)
        assertEquals(i*i, square(i));
  }
}
