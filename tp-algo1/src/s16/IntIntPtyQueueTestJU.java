package s16;

import org.junit.jupiter.api.Test;

import java.util.PriorityQueue;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

// ------------------------------------------------------------
public class IntIntPtyQueueTestJU {
  static record PQElt(int elt, int pty) implements Comparable<PQElt> {
    @Override public int compareTo(PQElt o) {
      return Integer.compare(this.pty, o.pty);
    }
  }
  //===============================================
  static Random rnd = new Random(2209);

  @Test void rndTest() {
    rnd = new Random(2209);
    int nOps = 10_000, maxPty = nOps/10;
    int nRepetitions = 10;
    for(int i=0; i<nRepetitions; i++)
      exercise(nOps, maxPty);
  }
  
  static void exercise(int nOps, int maxPty) {
    int p;
    IntIntPtyQueue pq = new IntIntPtyQueue(maxPty);
    PriorityQueue<PQElt> pq1 = new PriorityQueue<>();
    for(int i=0; i<nOps; i++) {
      p = rnd.nextInt(maxPty);
      pq.enqueue(p, p);
      pq1.add(new PQElt(p, p));
    }
    for (int i=0; i<nOps; i++) {
      assertEquals(pq1.element().elt, pq.consult());
      assertEquals(pq1.element().pty, pq.consultPty());
      assertEquals(pq1.remove().elt, pq.dequeue());
    }
    assertTrue(pq.isEmpty());
    for(int i=0; i<nOps; i++) {
      p = rnd.nextInt(maxPty); pq.enqueue(p, p);
      p = rnd.nextInt(maxPty); pq.enqueue(p, p);
      pq.dequeue();
    }
    while(!pq1.isEmpty()) {
      p = pq.dequeue();
      assertEquals(pq1.remove().elt, p);
    }
  }
}

