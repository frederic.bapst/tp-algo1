package s16;

import s10.IntQueueChained;

public class IntIntPtyQueue {
  private final IntQueueChained[] fifoQueues;
  //        or: IntQueueArray  [] fifoQueues;
  // ...

  // priorities will be in [0 .. nPriorities-1]
  public IntIntPtyQueue(int nPriorities) {
    fifoQueues = null;
    // TODO 
  }

  public boolean isEmpty() {
    return false; // TODO 
  }

  // PRE: 0 <= pty < nPriorities
  public void enqueue(int elt, int pty) {
    // TODO 
  }

  // strongest pty present in the queue.
  // PRE: ! isEmpty()
  public int consultPty() {
    return 0; // TODO 
  }

  // elt with strongest (=smallest) pty.  
  // PRE: ! isEmpty()
  public int consult() {
    return 0; // TODO 
  }

  // elt with strongest (=smallest) pty.  
  // PRE: ! isEmpty()
  public int dequeue() {
    return 0; // TODO 
  }
}

