package s14;

import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SubsetSumTestJU {

  static boolean subsetSumRec(int[] coins, int k) {
    return ssr(coins, k, coins.length-1);
  }
  static boolean ssr(int[] coins, int k, int withCoins) {
    if (k <0) return false;
    if (k==0) return true;
    if (withCoins<0) return false;
    if (ssr(coins, k,                  withCoins-1)) return true;
    if (ssr(coins, k-coins[withCoins], withCoins-1)) return true;
    return false;
  }

  @Test void subsetSumOnRndData() {
    int n = 7, r = 2_000;
    for(int i=0; i<r; i++) {
      int len = 1+rnd.nextInt(7);
      int[] t = rndData(n, rnd.nextBoolean());
      for(int k=0; k<n*n; k++) {
        boolean r1 = SubsetSum.subsetSumDyn(t, k);
        boolean r2 = subsetSumRec(t, k);
        assertEquals(r2, r1);
      }
    }
  }
  static Random rnd = new Random(2209);
  static int[] rndData(int n, boolean small) {
    int max = small ? n+1 : Short.MAX_VALUE;
    int[] t = new int[n];
    for(int i=0; i<n; i++)
      t[i] = rnd.nextInt(max);
    return t;
  }
}
