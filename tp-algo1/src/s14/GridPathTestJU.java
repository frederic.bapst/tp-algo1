package s14;

import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GridPathTestJU {

  @Test void bestPathOnRndData() {
    int n = 7, r = 10_000;
    for(int i=0; i<r; i++) {
      int len = 1+rnd.nextInt(7);
      int[][] t = rndData(n, rnd.nextBoolean());
      int r1 = GridPath.minPathDyn(t);
      int r2 = GridPath.minPath(t);
      assertEquals(r2, r1);
    }
  }
  static Random rnd = new Random(2209);
  static int[][] rndData(int n, boolean small) {
    int max = small ? n+1 : Short.MAX_VALUE;
    int[][] t = new int[n][n];
    for(int i=0; i<n; i++)
      for(int j=0; j<n; j++)
        t[i][j] = rnd.nextInt(max);
    return t;
  }
}
