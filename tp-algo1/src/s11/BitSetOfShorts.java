package s11;
import java.util.BitSet;

public class BitSetOfShorts {
  final BitSet bs;
  static final short MINV = Short.MIN_VALUE;
  static final short MAXV = Short.MAX_VALUE;

  static int indexFromElt(short e) {
    return 0; // TODO ...  
  }
  static short eltFromIndex(int i)   {
    return 0; // TODO ...  
  }

  public BitSetOfShorts()  {
    bs = new BitSet(); // or: new BitSet(1 + HIGH - LOW); 
  }
  // ------------------------------------------------------------
  public void add (short e) {
    // TODO ...
  } 
  public void remove(short e) {
     // TODO ...
  } 
  public boolean contains(short e) {
     return false; // TODO ...
  } 
  public BitSetOfShortsItr iterator() {
    return null; // TODO ...
  }

  public void union(BitSetOfShorts s) {
    // TODO ...
  } 
  public void intersection(BitSetOfShorts s) {
    // TODO ...
  } 

  public int size() {
    return 0; // TODO ...
  } 
  
  public boolean isEmpty() {
    return bs.length() == 0;
  }

  public String toString() { 
    String r = "{";
    BitSetOfShortsItr itr = iterator();
    if (isEmpty()) return "{}";
    r += itr.nextElement();
    while (itr.hasMoreElements()) {
      r += ", " + itr.nextElement();
    }
    return r + "}";
  }
  // ------------------------------------------------------------
  // ------------------------------------------------------------
  public static void main(String[] args) {
    BitSetOfShorts a = new BitSetOfShorts();
    BitSetOfShorts b = new BitSetOfShorts();
    short[] ta = {-3, 5, 6, -3, 9, 9};
    short[] tb = {6, 7, -2, -3};
    int i;
    for (i=0; i<ta.length; i++) {
      a.add(ta[i]);
      System.out.println("" + a + a.size());
    }
    for (i=0; i<tb.length; i++) {
      b.add(tb[i]);
      System.out.println("" + b + b.size());
    }
    a.union(b);
    System.out.println("" + a + a.size());
  }
}
